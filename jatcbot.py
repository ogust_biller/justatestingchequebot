"""
This is a echo bot.
It echoes any incoming text messages.
"""

import logging

from aiogram import Bot, Dispatcher, executor, types
from conf import settings
import urllib.request

from pyzbar.pyzbar import decode
from PIL import Image

api_token = settings.TG_API_TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=api_token)
dp = Dispatcher(bot)


#@dp.message_handler(commands=['photo'])
@dp.message_handler(content_types=types.ContentType.PHOTO)
async def process_photo_command(msg: types.Message):
    try:
        file_info = await bot.get_file(msg.photo[len(msg.photo)-1].file_id)
        fi = file_info.file_path
        urllib.request.urlretrieve(f'https://api.telegram.org/file/bot{api_token}/{fi}',f'./{fi}')
        cheque_info = decode(Image.open(f'./{fi}'))[0].data.decode('utf-8')
        await bot.send_message(msg.from_user.id,'Cheque Info string: '+cheque_info) 
        await bot.send_message(msg.from_user.id,'Bonus code: '+msg.caption)
    except Exception as e:
        await bot.send_message(msg,e )
    

#@dp.message_handler()
#async def echo(message: types.Message):
    # old style:
    # await bot.send_message(message.chat.id, message.text)

#    await message.answer(message.text)

@dp.message_handler()
async def echo_message(msg: types.Message):
    await bot.send_message(msg.from_user.id, msg.text)


@dp.message_handler(content_types=types.ContentType.ANY)
async def unknown_message(msg: types.Message):
    message_text = 'Я не знаю, что с этим делать \nЯ просто напомню, что есть команда /help'
    await msg.reply(message_text, parse_mode=types.message.ParseMode.MARKDOWN)



if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)